#ifndef FIFO_H
#define FIFO_H

#include <stdio.h>
#include "packet.h"

typedef struct node node_t;
struct node {
	data_t data;
	node_t *prev;
};

typedef struct fifo fifo_t;
struct fifo {
	node_t *first;
	node_t *last;	
	uint8_t length;
};

fifo_t* fifo_init();

void enfile(fifo_t *fifo, data_t *new_data);
void defile(fifo_t *fifo);

data_t* copy_to_buffer(fifo_t *fifo, uint8_t* size);
void copy_to_fifo(fifo_t *fifo, data_t *data, uint8_t length);

#endif
