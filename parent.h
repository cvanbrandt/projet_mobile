#ifndef __PARENT_H
#define __PARENT_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h> /* uintx_t */
#include <string.h>

#include "net/rime.h"

typedef struct config config_t;
struct config {
	uint8_t mode; // 0 for periodic, 1 for differential
	uint8_t send_data;
	uint8_t decision; // 0 for hops, 1 for signal strength
	uint32_t version;
};

typedef struct parent parent_t;
struct parent {
	rimeaddr_t address;
	uint8_t hops;
	uint8_t active;
	uint8_t token;
	uint32_t signal_strength;
};

void printConfig(const config_t *config);

#endif
