/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Testing the broadcast layer in Rime
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include "contiki.h"
#include "net/rime.h"
#include "random.h"

#include "dev/button-sensor.h"
#include "dev/leds.h"
#include "dev/serial-line.h"

#include <stdio.h>
//#include "parent.h"
#include "packet.h"

/*---------------------------------------------------------------------------*/
PROCESS(neighbor_discovery, "Neighbor Discovery");
PROCESS(unicast, "Unicast");
PROCESS(serial_communication, "Serial Communication");
AUTOSTART_PROCESSES(&neighbor_discovery, &unicast, &serial_communication);
/*---------------------------------------------------------------------------*/

void printConfig(const config_t *config)
{
	printf("Config v%u : %u%u%u\n", config->version, config->mode, config->send_data, config->decision);
}

static config_t config;

/*---------------------------------------------------------------------------*/
/* BROADCAST */
/*---------------------------------------------------------------------------*/

static void broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
	//printf("broadcast message received from %d.%d: '%s'\n", from->u8[0], from->u8[1], (char *)packetbuf_dataptr());
	//node_t* node = get_node(neighbor_list, from);
	//hello_pkt_t* packet = (hello_pkt_t*) packetbuf_dataptr();
}

static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static struct broadcast_conn broadcast;

/* MAIN */

PROCESS_THREAD(neighbor_discovery, ev, data)
{
	static struct etimer et;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	/* always broadcasts hello messages */

	while(1) {

		/* Delay 8-12 seconds */ 
		etimer_set(&et, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));

		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

		hello_pkt_t* hello = malloc(sizeof(hello_pkt_t));
		hello->type = HELLO;
		hello->hops = 64;
		packetbuf_copyfrom((char*) hello, sizeof(hello_pkt_t));
		free(hello);
				
		broadcast_send(&broadcast);
	
		printf("Hello message sent\n");
	}
	PROCESS_END();
}

/*---------------------------------------------------------------------------*/
/* UNICAST */
/*---------------------------------------------------------------------------*/

static struct runicast_conn runicast; 

static void recv_runicast(struct runicast_conn *c, const rimeaddr_t *from, uint8_t seqno)
{
	static uint32_t prev_seq = 0;
	printf("runicast message received from %d.%d, seqno %d\n", from->u8[0], from->u8[1], seqno);
	void *message =	packetbuf_dataptr();
	uint8_t type = *((uint8_t*) message);
	if(type == CONFIG_REQ){
		config_req_pkt_t* config_req_pkt = (config_req_pkt_t*) message;
		if(config_req_pkt->version < config.version) { /* replies to the config request with the new config */
			config_pkt_t* config_pkt = malloc(sizeof(config_pkt_t));
			config_pkt->type = CONFIG;
			config_pkt->config = config;
			packetbuf_copyfrom(config_pkt, sizeof(config_pkt_t));
			runicast_send(&runicast, from, MAX_RETRANSMISSION);
			free(config_pkt);
		}
	}
	else if (type == DATA) {
		uint8_t length = *((uint8_t*) message + sizeof(uint8_t));
		uint32_t seq = *((uint8_t*) message + sizeof(uint8_t)*2);
		if(seq != prev_seq){ /* not a duplicate */
			/* decode the data received and prints them */
			data_t* data = malloc(sizeof(data_t) * length);
			memcpy(data, message+sizeof(uint8_t)*2 + sizeof(uint32_t), sizeof(data_t)*length);
			int i;
			for(i=0; i < length; ++i)
				printf("Data/%u/%d/%d/%d/\n", data[i].origin.u8[0], data[i].temperature, data[i].humidity, data[i].pressure);
			prev_seq = seq;
		}
	}
}

static void sent_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
	printf("runicast message sent to %d.%d, retransmissions %d\n", to->u8[0], to->u8[1], retransmissions);
}

static void timedout_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
	printf("runicast message timed out when sending to %d.%d, retransmissions %d\n", to->u8[0], to->u8[1], retransmissions);
}

static const struct runicast_callbacks runicast_callbacks = {recv_runicast,
							     sent_runicast,
							     timedout_runicast};

/* MAIN */

PROCESS_THREAD(unicast, ev, data)
{
	PROCESS_EXITHANDLER(runicast_close(&runicast));

	PROCESS_BEGIN();

	runicast_open(&runicast, 144, &runicast_callbacks);

	//SENSORS_ACTIVATE(button_sensor);

	while(1) {
		PROCESS_WAIT_EVENT();
	}
	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/* Serial Communication */
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(serial_communication, ev, data)
{
	PROCESS_BEGIN();

	/* inital values for the config */

	config.version = 1;
	config.send_data = 0;
	config.mode = 0;
	config.decision = 0;

	while(1) {
		PROCESS_YIELD();
		
		if(ev == serial_line_event_message) {
			char* str = (char*) data;

			printf("%s\n", str);
			if(str[0] == 'C'){ /* Received config modif from the gateway */
				++config.version;
				config.mode = str[1] - '0';
				config.decision = str[2] - '0';
				printConfig(&config);		
			}
			else { /* Received the number of subscriber from the gateway */
				++config.version;
				int number = str[0] - '0';
				config.send_data = number > 2;
			}
		}
	}

	PROCESS_END();
}
