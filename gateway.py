from subprocess import Popen, PIPE, call
import os
import io
import sys
import threading
import select
from time import sleep

# Disclaimer : this code is inspired by the code of a friend of ours who did the project last year. Credits : Razvan Alexandru Ursu and his binome at the time.

def MoteInterface(process):
	#the stdout is read continously
	while True:
		ready,_,_ = select.select([process.stdout, sys.stdin], [], [], 0.2)
		for x in ready:
			if x == process.stdout: #The root node prints us data
				output = process.stdout.readline()
				if output == '' and process.poll() is not None:
					break
				#If the string read starts by Data, so it contains measurements
				elif output[:4]==b'Data':
					output = output.decode("utf-8")
					#print(output)
					#The data are retrieved
					_,src,temp,hum,press,_ = output.split("/")
					#The data are published
					call(["mosquitto_pub","-t","Data/node"+src+"/temperature","-m","Node"+src+" temperature is: "+temp+"degrees C"])
					call(["mosquitto_pub","-t","Data/node"+src+"/humidity","-m","Node"+src+" humidity is: "+hum+"%"])
					call(["mosquitto_pub","-t","Data/node"+src+"/pressure","-m","Node"+src+" pressure is: "+press+"hPa\n"])
				#else:
				#	output = output.decode("utf-8")
				#	print(output)
			elif x == sys.stdin:
				line = sys.stdin.readline()
				#print(line)
				if line[:1] == 'C': #we are modifying the config of the tree
					process.stdin.write(line.encode("utf-8"))

#A pipe is created between the gateway and the root mote
os.chdir("/home/user/Documents/projet_mobile")
process = Popen(["make","login","TARGET=z1", "MOTES="+sys.argv[1]], stdout = PIPE, stdin = PIPE)
#The gateway is a subscriber of the number of actives clients and informs the root mote
process_count = Popen(["mosquitto_sub","-t","$SYS/broker/clients/active"], stdout = process.stdin, stdin = PIPE)
sleep(1)
#the publishement process starts
threading.Thread(target=MoteInterface(process)).start()


