#ifndef PACKET_H
#define PACKET_H

#define MAX_NUMBER_OF_TOKEN 3
#define MAX_RETRANSMISSION 5
#define DATA_BUFFER_SIZE 10

#include "parent.h"

enum {HELLO, CONFIG, CONFIG_REQ, DATA};

typedef struct hello_pkt hello_pkt_t;
struct hello_pkt {
	uint8_t type;
	uint8_t hops;
};


typedef struct config_pkt config_pkt_t;
struct config_pkt {
	uint8_t type;
	config_t config;
};

typedef struct config_req_pkt config_req_pkt_t;
struct config_req_pkt {
	uint8_t type;
	uint32_t version;
};

typedef struct data data_t;
struct data {
	rimeaddr_t origin;
	int temperature;
	int humidity;
	int pressure;
};

#endif
