/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Testing the broadcast layer in Rime
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include "contiki.h"
#include "net/rime.h"
#include "random.h"
#include "node-id.h"
#include "dev/cc2420.h"
#include "dev/cc2420_const.h"

#include "dev/button-sensor.h"

#include "dev/leds.h"

#include <stdio.h>
//#include "parent.h"
//#include "packet.h"
#include "fifo.h"

/*---------------------------------------------------------------------------*/
PROCESS(neighbor_discovery, "Neighbor Discovery");
PROCESS(unicast, "Unicast");
PROCESS(data_acquisition, "Data Acquisition");
AUTOSTART_PROCESSES(&neighbor_discovery, &unicast, &data_acquisition);
/*---------------------------------------------------------------------------*/

void printConfig(const config_t *config)
{
	printf("Config v%u : %u%u%u\n", config->version, config->mode, config->send_data, config->decision);
}

#define MAX_TOKEN 3

static parent_t *parent;

static config_t config;
static fifo_t *data_fifo;

/*---------------------------------------------------------------------------*/
/* BROADCAST */
/*---------------------------------------------------------------------------*/

static void broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
	//printf("broadcast message received from %d.%d: '%s'\n", from->u8[0], from->u8[1], (char *) packetbuf_dataptr());
	hello_pkt_t* packet = (hello_pkt_t*) packetbuf_dataptr();
	uint32_t signal_strength = cc2420_last_rssi; 
	//printf("%u : %d\n", from->u8[0], signal_strength);
	/* Decision process for change of parents and token update */
	if((!config.decision && packet->hops > parent->hops) || !parent->active || (from->u8[0] == parent->address.u8[0] && from->u8[1] == parent->address.u8[1]) || (config.decision && signal_strength > parent->signal_strength) ) {
		parent->address = *from;
		parent->hops = packet->hops;
		parent->active = 1;
		parent->signal_strength = signal_strength;
		parent->token = 0;
	}
	//printf("My parent is %u.%u\n", parent->address.u8[0], parent->address.u8[1]);
}

static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static struct broadcast_conn broadcast;

/* MAIN */

PROCESS_THREAD(neighbor_discovery, ev, data)
{
	static struct etimer hello_timer;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	parent = malloc(sizeof(parent_t));

	etimer_set(&hello_timer, CLOCK_SECOND * 10 + random_rand() % (CLOCK_SECOND * 5));

	while(1) {
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&hello_timer));

		/* sends hello message if parent is active */
		if(etimer_expired(&hello_timer)){
			if(parent->active){
				++parent->token;
				if(parent->token > MAX_TOKEN) /* token expired => parent not active anymore */
					parent->active = 0;
				else{
					hello_pkt_t* hello = malloc(sizeof(hello_pkt_t));
					hello->type = HELLO;
					hello->hops = parent->hops-1;
					packetbuf_copyfrom((char*) hello, sizeof(hello_pkt_t));
					free(hello);
				
					broadcast_send(&broadcast);
					printf("Hello message sent\n");
				}
			}
			etimer_reset(&hello_timer);
		}
	}
	
	PROCESS_END();
}

/*---------------------------------------------------------------------------*/
/* UNICAST */
/*---------------------------------------------------------------------------*/

static struct runicast_conn runicast; 

static void recv_runicast(struct runicast_conn *c, const rimeaddr_t *from, uint8_t seqno)
{
	static uint32_t prev_seq = 0;
	//printf("runicast message received from %d.%d, seqno %d\n", from->u8[0], from->u8[1], seqno);
	void *message =	packetbuf_dataptr();
	uint8_t type = *((uint8_t*) message);
	if(type == CONFIG_REQ){
		//printf("%u asked for config\n", from->u8[0]);
		config_req_pkt_t* config_req_pkt = (config_req_pkt_t*) message;
		if(config_req_pkt->version < config.version) {
			config_pkt_t* config_pkt = malloc(sizeof(config_pkt_t));
			config_pkt->type = CONFIG;
			config_pkt->config = config;
			packetbuf_copyfrom(config_pkt, sizeof(config_pkt_t));
			runicast_send(&runicast, from, MAX_RETRANSMISSION);
			free(config_pkt);
		}
	}
	else if (type == CONFIG) {
		/* copies the config received, no check for version number because parent always has more up to date version */
		config_pkt_t* config_pkt = (config_pkt_t*) message;
		config = config_pkt->config;
		printConfig(&config);
	}
	else if (type == DATA) {
		/* Decodes the data and copies them in the buffer*/
		uint8_t length;
		uint32_t seq;
		memcpy(&length, message+sizeof(uint8_t), sizeof(uint8_t));
		memcpy(&seq, message+sizeof(uint8_t)*2, sizeof(uint32_t));
		if(seq != prev_seq){
			data_t* data = malloc(sizeof(data_t) * length);
			memcpy(data, message+sizeof(uint8_t)*2 + sizeof(uint32_t), sizeof(data_t)*length);
			copy_to_fifo(data_fifo, data, length);
			prev_seq = seq;
		}
	}
}

static void sent_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
	printf("runicast message sent to %d.%d, retransmissions %d\n", to->u8[0], to->u8[1], retransmissions);
}

static void timedout_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
	printf("runicast message timed out when sending to %d.%d, retransmissions %d\n", to->u8[0], to->u8[1], retransmissions);
}

static const struct runicast_callbacks runicast_callbacks = {recv_runicast,
							     sent_runicast,
							     timedout_runicast};

/* MAIN */

PROCESS_THREAD(unicast, ev, data)
{
	PROCESS_EXITHANDLER(runicast_close(&runicast));

	PROCESS_BEGIN();

	runicast_open(&runicast, 144, &runicast_callbacks);

	static struct etimer config_timer;

	etimer_set(&config_timer, CLOCK_SECOND * 30 + random_rand() % (CLOCK_SECOND * 5));

	while(1) {
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&config_timer));
		
		//printf("Config timer request expired\n");

		/* asking for config periodically if we have an active parent */
		if(parent->active){
			if(!runicast_is_transmitting(&runicast)) {
				config_req_pkt_t *pkt = malloc(sizeof(config_req_pkt_t));
				pkt->version = config.version;
				pkt->type = CONFIG_REQ;
				packetbuf_copyfrom(pkt, sizeof(config_req_pkt_t));
				runicast_send(&runicast, &parent->address, MAX_RETRANSMISSION);
				etimer_reset(&config_timer);
			}
		}
		else
			etimer_reset(&config_timer);
	}
	PROCESS_END();
}

/*---------------------------------------------------------------------------*/
/* DATA ACQUISITION */
/*---------------------------------------------------------------------------*/

int data_comparison(const data_t old_data, const data_t new_data, const int threshold)
{
	return (abs(old_data.humidity - new_data.humidity) > threshold) || (abs(old_data.temperature - new_data.temperature) > threshold) || (abs(old_data.pressure - new_data.pressure) < threshold);
}

PROCESS_THREAD(data_acquisition, ev, data)
{
	PROCESS_BEGIN();

	static struct etimer acquisition_timer;
	static struct etimer transmission_timer;

	etimer_set(&acquisition_timer, CLOCK_SECOND * 20 + random_rand() % (CLOCK_SECOND * 4));
	etimer_set(&transmission_timer, CLOCK_SECOND * 30 + random_rand() % (CLOCK_SECOND * 10));

	static data_t old_data, new_data;
	old_data.humidity = 0;
	old_data.temperature = 0;
	old_data.pressure = 0;

	config.version = 0;
	config.send_data = 0;
	config.mode = 0;
	config.decision = 0;

	data_fifo = fifo_init();

	while(1) {
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&acquisition_timer) || etimer_expired(&transmission_timer));

		if(etimer_expired(&acquisition_timer)){
			/* generate new data */
			new_data.temperature = random_rand() % 30 + 15;
			new_data.humidity = random_rand() % 50;
			new_data.pressure = random_rand() % 1000;
			new_data.origin = rimeaddr_node_addr;
			if((config.mode && data_comparison(old_data, new_data, 0)) || !config.mode){
				enfile(data_fifo, &new_data);
				if(data_fifo->length > DATA_BUFFER_SIZE) // delete old data if the buffer size becomes too large
					defile(data_fifo);
			}
			etimer_reset(&acquisition_timer);
		}

		if(config.send_data && parent->active){
			if(etimer_expired(&transmission_timer) && !runicast_is_transmitting(&runicast)) {
				// we should send our data buffer
				// Data format : type (uint8_t), length (uint8_t), seqnum (uint32_t), data
				uint8_t type = DATA;
				uint8_t length;
				data_t* data = copy_to_buffer(data_fifo, &length);
				void* buff = malloc(2*sizeof(uint8_t) + sizeof(uint32_t) + sizeof(data_t) * length);
				uint32_t seq = random_rand() % 1000000 + 1;
				// We fill the buffer element by element
				memcpy(buff, &type, sizeof(uint8_t));
				memcpy(buff + sizeof(uint8_t), &length, sizeof(uint8_t));
				memcpy(buff + 2*sizeof(uint8_t), &seq, sizeof(uint32_t));
				memcpy(buff + 2*sizeof(uint8_t) + sizeof(uint32_t), data, sizeof(data_t)*length);

				packetbuf_copyfrom(buff, 2*sizeof(uint8_t) + sizeof(uint32_t) + sizeof(data_t) * length);
				
				printf("Sending data\n");
				runicast_send(&runicast, &parent->address, MAX_RETRANSMISSION);

				etimer_reset(&transmission_timer);
			}
		}

		old_data = new_data;
	}
	PROCESS_END();
}
