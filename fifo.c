#include "fifo.h"

fifo_t* fifo_init()
{
	fifo_t* fifo = malloc(sizeof(fifo_t));
	fifo->first = NULL;
	fifo->last = NULL;
	fifo->length = 0;
}

/* Adding some data to the fifo */
void enfile(fifo_t *fifo, data_t *new_data)
{
	node_t *new_node = malloc(sizeof(node_t));
	new_node->data = *new_data;
	new_node->prev = NULL;

	if(fifo->length == 0){
		fifo->first = new_node;
	}
	else {
		fifo->last->prev = new_node;
	}
	fifo->last = new_node;
	++fifo->length;
}

/* Deleting first data in the file*/
void defile(fifo_t *fifo)
{
	if(fifo->length > 0){
		node_t *temp = fifo->first->prev;
		free(fifo->first);
		fifo->first = temp;
		--fifo->length;
	}
}

/* Allocates and fill a buffer */
data_t* copy_to_buffer(fifo_t *fifo, uint8_t* size)
{
	*size = fifo->length;
	data_t* buffer = NULL;
	if(fifo->length > 0){
		buffer = malloc(sizeof(data_t) * fifo->length);
		int i;
		for(i = 0; fifo->first != NULL && i < *size; ++i){
			buffer[i] = fifo->first->data;
			defile(fifo);
		}
	}
	return buffer;
}

void copy_to_fifo(fifo_t *fifo, data_t *data, uint8_t length)
{
	int i;
	for(i=0; i<length; ++i){
		enfile(fifo, &data[i]);
	}
}
